ARG TAG=latest
FROM quay.io/centos/centos:${TAG}

RUN echo "install_weak_deps=False" >> /etc/dnf/dnf.conf

RUN dnf update -y \
 && dnf install -y dracut \
 && ln -sf /usr/bin/true /usr/bin/dracut \
 && dnf install -y kernel systemd \
 && mv /usr/lib/modules/*/vmlinuz /boot/wyrcan.kernel \
 && systemctl unmask console-getty.service dbus-org.freedesktop.login1.service systemd-logind.service getty.target \
 && dnf clean all

# Unfortunately, we have to disable selinux due to labeling problems.
RUN echo "selinux=0" > /boot/wyrcan.cmdline
RUN ln -s /lib/systemd/systemd /init
